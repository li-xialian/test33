package com.lxl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxl.model.po.TbContorl;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2023-07-31
 */
public interface TbContorlMapper extends BaseMapper<TbContorl> {

}
