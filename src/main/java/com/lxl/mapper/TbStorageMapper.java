package com.lxl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxl.model.po.TbStorage;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
public interface TbStorageMapper extends BaseMapper<TbStorage> {

}
