package com.lxl.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lxl.model.po.TbRealt;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
public interface TbRealtMapper extends BaseMapper<TbRealt> {

}
