package com.lxl.controller;

import com.alibaba.fastjson.JSON;
import com.lxl.model.dto.chat.CharDto;
import com.lxl.service.ContentInter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class ChatControl {
    @Autowired
    ContentInter contentInter;


    @GetMapping("/chat1")
    public CharDto getchat1() {
        CharDto chat = contentInter.chat1(6);
        return chat;
    }

    @GetMapping("/chat2")
    public CharDto getchat2() {
        CharDto chat = contentInter.chat2(6);
       return chat;
    }


}
