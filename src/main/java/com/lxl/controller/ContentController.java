package com.lxl.controller;

import ch.qos.logback.core.joran.util.beans.BeanUtil;
import com.lxl.model.dto.CAuto;
import com.lxl.model.dto.CHand;
import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.dto.PublishDto;

import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.po.*;
import com.lxl.mqtt.MqttMessageService;
import com.lxl.service.ContentInter;
import com.lxl.service.TbSave;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lxl
 */
@Slf4j
@RestController
@CrossOrigin //跨域问题
@RequestMapping("/lxl")
public class ContentController {

    @Autowired
    ContentInter contentInter;
    @Autowired
    MqttMessageService mqttMessageService;
    @Autowired
    TbSave tbSave;

    private String topic="mqtt/Control" ;


    @GetMapping("/storage")
    public  List<TbStorage> getTbStorage() {
        List<TbStorage> tbStorage = contentInter.getTbStorage();
        return  tbStorage;

    }


    @PostMapping("/realLight")
    public  PageResult<TbReall> getRealLight(@RequestBody DateTimeDtos dateTimeDtos) {

        PageResult<TbReall> p = contentInter.getRealLight(dateTimeDtos);
        List<TbReall> items = p.getItems();
        return p;

    }
    @PostMapping("/realTemperature")
    public  PageResult<TbRealt> getRealTemperature(@RequestBody DateTimeDtos dateTimeDtos) {
        PageResult<TbRealt> p = contentInter.getRealTemperature(dateTimeDtos);
        List<TbRealt> items = p.getItems();
        return p;

    }

    @PostMapping("/vStatus")
    public List<TbVstatus> getVStatus(@RequestBody DateTimeDtos dateTimeDtos) {
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        List<TbVstatus> vStatus = contentInter.getVStatus(start, end);
        return vStatus;

    }

    @PostMapping("/send")
    public List<TbContorl> sendcommand(@RequestBody PublishDto publishDto) {
        if(publishDto.getMode()!=null&&!publishDto.getMode().equals("")) {

            mqttMessageService.publish(topic, publishDto);
            LocalDateTime localTime = LocalDateTime.now();


            TbContorl tbContorl = new TbContorl();
            BeanUtils.copyProperties(publishDto, tbContorl);

            TbContorl tb = new TbContorl();
            if(tbContorl.getMode().equals("mode_hand")){
                CHand cHand = new CHand();
                BeanUtils.copyProperties(tbContorl, cHand);
                BeanUtils.copyProperties(cHand, tb);
            }
            if(tbContorl.getMode().equals("mode_auto")){
                CAuto cAuto = new CAuto();
                BeanUtils.copyProperties(tbContorl, cAuto);
                BeanUtils.copyProperties(cAuto, tb);
            }
            tb.setTime(localTime);
            tbSave.saveControl(tb);
        }
        List<TbContorl> contorl = contentInter.getContorl();
        return contorl;

    }
    @PostMapping("/sendTime")
    public boolean sendTime(@RequestBody DateTimeDtos dateTimeDtos){
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        //转换格式
        String s="";
        String[] split = start.split(":");
        s="h1:"+split[0]+",m1:"+split[1]+",";
        String[] split1 = end.split(":");
        s=s+"h2:"+split1[0]+",m2:"+split1[1];
        System.out.println(s);
        mqttMessageService.publish(topic, s);
        return true;
    }


}
