package com.lxl.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class CAuto {
    private String mode;
    private String tempMax;
    private String tempMin;
    private String lightMiddle;
    private Integer flag;//0为关的，1为开的
}
