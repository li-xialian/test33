package com.lxl.model.dto.chat;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Data
@Slf4j
public class SeriesDto {
 private List<Float> data;
 private String name;
}
