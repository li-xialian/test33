package com.lxl.model.dto.pages;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * @description 分页查询结果模型
 * @param <T> 泛型因为不同的业务对象数据列表不一样
 */
@Data
@ToString
public class PageResult<T> implements Serializable {

    // 数据列表
    private List<T> items;

    //总记录数
    private long total;

    //当前页码
    private long pageCurrent;

    //每页记录数
    private long pageSize;

    public PageResult(List<T> items, long counts, long page, long pageSize) {
        this.items = items;
        this.total = counts;
        this.pageCurrent = page;
        this.pageSize = pageSize;
    }
    public PageResult(List<T> items) {
        this.items = items;

    }

}