package com.lxl.model.dto.pages;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class PageParams {
    //当前页码
    private Long pageSize = 1L;

    //每页记录数默认值
    private Long pageCurrent = 10L;

    public PageParams() {

    }

    public PageParams(long pageNo, long pageSize) {
        this.pageCurrent = pageCurrent;
        this.pageSize = pageSize;
    }
}
