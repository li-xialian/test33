package com.lxl.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

@Data
@ToString
public class PublishDto {
    private String mode;
    private String tempMax;
    private String tempMin;
    private String lightMiddle;
    private String fan1OnOff;
    private String fan1;
    private String fan2OnOff;
    private String fan2;
    private String ledOnOff;
    private String led;
    private LocalDateTime time;
    private Integer flag;//0为关的，1为开的
}
