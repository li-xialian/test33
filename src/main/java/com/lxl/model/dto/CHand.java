package com.lxl.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

@Data
public class CHand {
    private String mode;
    private String fan1OnOff;

    private String fan1;

    private String fan2OnOff;

    private String fan2;

    private String ledOnOff;
    private Integer flag;//0为关的，1为开的

    private String led;
}
