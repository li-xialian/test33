package com.lxl.model.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PathVariable;

import java.time.LocalDateTime;

@Data
@Slf4j
public class DateTimeDtos {

    private String start;

    private String end;
    private Integer pageSize;
    private Integer pageCurrent;
}
