package com.lxl.model.dto;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RealtDto {
    private Float tin;
    private Float tout;
    private Long time;
}
