package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbStorage implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 储运物品编号
     */
    @TableField("PID")
    private Long pid;


}
