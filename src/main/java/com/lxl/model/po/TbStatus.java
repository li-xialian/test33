package com.lxl.model.po;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备状态信息
     */
    private Integer status;

    /**
     * 状态类型
     */
    private String type;


}
