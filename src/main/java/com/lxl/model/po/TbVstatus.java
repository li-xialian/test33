package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbVstatus implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 设备状态信息
     */
    @TableField("Status")
    private Integer status;

    /**
     * 时间
     */
    @TableField("StatusTime")
    private LocalDateTime statustime;


}
