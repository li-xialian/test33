package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbRealt implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * V内部实时温度
     */
    @TableField("Tin")
    private Float tin;

    /**
     * V外部实时温度
     */
    @TableField("Tout")
    private Float tout;

    /**
     * 时间
     */
    @TableField("Time")
    private LocalDateTime time;


}
