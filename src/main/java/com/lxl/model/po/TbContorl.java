package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-07-31
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbContorl implements Serializable {

    private static final long serialVersionUID = 1L;

    private String mode;
    @TableField("temp_max")
    private String tempMax;
    @TableField("temp_min")
    private String tempMin;
    @TableField("light_middle")
    private String lightMiddle;

    @TableField("fan1_ON_OFF")
    private String fan1OnOff;

    private String fan1;

    @TableField("fan2_ON_OFF")
    private String fan2OnOff;

    private String fan2;

    @TableField("led_ON_OFF")
    private String ledOnOff;

    private String led;
    private Integer flag;

    private LocalDateTime time;



}
