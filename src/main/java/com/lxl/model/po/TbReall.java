package com.lxl.model.po;

import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lxl
 * @since 2023-07-27
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class TbReall implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 实时光照
     */
    @TableField("Lxin")
    private Float lxin;

    /**
     * 时间
     */
    @TableField("Time")
    private LocalDateTime time;


}
