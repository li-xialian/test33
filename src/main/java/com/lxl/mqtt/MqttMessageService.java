package com.lxl.mqtt;

import org.springframework.stereotype.Component;

@Component
public interface MqttMessageService {
    void publish(String topic, String message);
    <T> void publish(String topic, T entity);
}
