package com.lxl.mqtt;

import com.alibaba.fastjson.JSON;
import com.lxl.model.dto.PublishDto;
import com.lxl.model.po.TbContorl;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.stereotype.Component;

/**
 * MQTT工具类操作
 *
 * @author Mr.Qu
 * @since  2020/11/18
 */
@Slf4j
@Component
public class MQTTConnect {

    //mqtt://broker.emqx.io/1883
    //wc://127.0.0.1:1883/mqtt
    //tcp://10.0.38.11:1883
    //tcp://127.0.0.1:1883
    //tcp://192.168.137.1:1883
    //hngc05:tcp://192.168.1.101:1883
    //iqoo: tcp://192.168.254.192:1883
    //  @Value("${mqtt.broker}")
    private String HOST = "tcp://192.168.254.192:1883";
    private final String clientId = "Client" + (int) (Math.random() * 100000000);
    private MqttClient mqttClient;

    /**
     * 客户端connect连接mqtt服务器
     *
     * @param username 用户名
     * @param password 密码
     * @param mqttCallback 回调函数
     **/
    public void setMqttClient(String username, String password, MqttCallback mqttCallback)
            throws MqttException {
        MqttConnectOptions options = mqttConnectOptions(username, password);
        /*if (mqttCallback == null) {
            mqttClient.setCallback(new Callback());
        } else {
        }*/
        mqttClient.setCallback(mqttCallback);
        mqttClient.connect(options);
    }

    /**
     * 客户端connect连接mqtt服务器
     *
     * @param username 用户名
     * @param password 密码
     **/
    public void setMqttClient(String username, String password)
            throws MqttException {
        MqttConnectOptions options = mqttConnectOptions(username, password);
        /*if (mqttCallback == null) {
            mqttClient.setCallback(new Callback());
        } else {
        }*/
        mqttClient.connect(options);
    }

    /**
     * 客户端connect设置回调函数
     *
     * @param mqttCallback 回调函数
     **/
    public void setMqttClient(MqttCallback mqttCallback)
            throws MqttException {

        /*if (mqttCallback == null) {
            mqttClient.setCallback(new Callback());
        } else {
        }*/
        mqttClient.setCallback(mqttCallback);
    }


    /**
     * MQTT连接参数设置
     */
    private MqttConnectOptions mqttConnectOptions(String userName, String passWord)
            throws MqttException {
        mqttClient = new MqttClient(HOST, clientId, new MemoryPersistence());
        MqttConnectOptions options = new MqttConnectOptions();
        options.setUserName(userName);
        options.setPassword(passWord.toCharArray());
        options.setConnectionTimeout(10);///默认：30
        options.setAutomaticReconnect(true);//默认：false
        options.setCleanSession(false);//默认：true
        //options.setKeepAliveInterval(20);//默认：60
        return options;
    }

    /**
     * 关闭MQTT连接
     */
    public void close() throws MqttException {
        mqttClient.close();
        mqttClient.disconnect();
    }

    /**
     * 向某个主题发布消息 默认qos：1
     */
    public void pub(String topic, String msg) {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(2);
        MqttTopic mqttTopic = mqttClient.getTopic(topic);
        mqttMessage.setPayload(msg.getBytes());
        try {
            mqttTopic.publish(mqttMessage);
        } catch (MqttException e) {
            throw new RuntimeException(e);
        }
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 向某个主题发布消息
     *
     * @param topic: 发布的主题
     * @param msg: 发布的消息
     * @param qos: 消息质量    Qos：0、1、2
     */
    public void pub(String topic, String msg, int qos)  {
        MqttMessage mqttMessage = new MqttMessage();
        mqttMessage.setQos(1);
        MqttTopic mqttTopic = mqttClient.getTopic(topic);

        try {
//        mqttMessage.setPayload(msg.getBytes());
//        MqttDeliveryToken token = mqttTopic.publish(mqttMessage);
//        token.waitForCompletion();
            //一条一条发
            PublishDto tbContorl = JSON.parseObject(msg, PublishDto.class);

            //Mode
            if (0==tbContorl.getFlag()) {
                mqttMessage.setPayload((tbContorl.getMode()).getBytes());
                mqttTopic.publish(mqttMessage);
                Thread.sleep(1000);
            }
            //0：自动
            if (!tbContorl.getMode().equals("mode_hand")) {

                String tempMax = tbContorl.getTempMax();

                if (!tempMax.equals("")) {
                    mqttMessage.setPayload(("temp_max:" + tempMax).getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);
                }
                String tempMin = tbContorl.getTempMin();
                if (!tempMin.equals("")) {
                    mqttMessage.setPayload(("temp_min:" + tempMin).getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);
                }
                String lightMiddle = tbContorl.getLightMiddle();
                if (!lightMiddle.equals("")) {
                    mqttMessage.setPayload(("light_middle:" + lightMiddle).getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);
                }
            } else {
                //手动模式
                //风机
                String fan1_on_off = tbContorl.getFan1OnOff();
                if (!fan1_on_off.equals("")) {
//                mqttMessage.setPayload((fan1_on_off).getBytes());
//                mqttTopic.publish(mqttMessage).waitForCompletion();
//                Thread.sleep(1000);


                    if (fan1_on_off.equals("fan1_on")) {
                        mqttMessage.setPayload(("fan1:" + tbContorl.getFan1()).getBytes());
                        mqttTopic.publish(mqttMessage);
                        Thread.sleep(1000);
                    }
                }
                //空调
                String fan2_on_off = tbContorl.getFan2OnOff();
                if (!fan2_on_off.equals("")) {
//                mqttMessage.setPayload((fan2_on_off).getBytes());
//                mqttTopic.publish(mqttMessage).waitForCompletion();
//                Thread.sleep(1000);


                    if (fan2_on_off.equals("fan2_on")) {
                        mqttMessage.setPayload(("fan2:" + tbContorl.getFan2()).getBytes());
                        mqttTopic.publish(mqttMessage);
                        Thread.sleep(1000);
                    }
                }

                //灯光

                String led_on_off = tbContorl.getLedOnOff();
                if (!led_on_off.equals("")) {
                    mqttMessage.setPayload((led_on_off).getBytes());
                    mqttTopic.publish(mqttMessage);
                    Thread.sleep(1000);

                    if (led_on_off.equals("led_on")) {
                        mqttMessage.setPayload(("led:" + tbContorl.getLed()).getBytes());
                        mqttTopic.publish(mqttMessage);
                        Thread.sleep(1000);
                    }
                }


            }
        }catch (Exception e){System.out.println("lxl:连接客户机异常");
        }

    }

    /**
     * 订阅某一个主题 ，此方法默认的的Qos等级为：1
     *
     * @param topic 主题
     */
    public void sub(String topic) throws MqttException {
        mqttClient.subscribe(topic);
    }

    /**
     * 订阅某一个主题，可携带Qos
     *
     * @param topic 所要订阅的主题
     * @param qos 消息质量：0、1、2
     */
    public void sub(String topic, int qos) throws MqttException {
        mqttClient.subscribe(topic, qos);
    }

    public static void main(String[] args) throws MqttException {
//    MQTTConnect mqttConnect = new MQTTConnect();
//    String msg = "Mr.Qu" + (int) (Math.random() * 100000000);
//
//    mqttConnect.sub("com/iot/init");
//    mqttConnect.pub("com/iot/init", msg);


    }
}
