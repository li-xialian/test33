package com.lxl.mqtt;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.lxl.mapper.TbContorlMapper;
import com.lxl.model.dto.ReallDto;
import com.lxl.model.dto.RealtDto;
import com.lxl.model.po.*;
import com.lxl.service.ContentInter;
import com.lxl.service.TbSave;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 回调函数
 */
@Slf4j
@Component
public class InitCallback implements MqttCallback {

    @Autowired
    TbSave tbSave;
    private String messageContent;
    //断网用的
    @Autowired
    ContentInter contentInter;


    private static LocalDateTime end=LocalDateTime.now();
    private static LocalDateTime now = LocalDateTime.now();



    @Override
    public void connectionLost(Throwable cause) {
        System.out.println("connectionLost: " + cause.getMessage());
    }

    @Override
    public void messageArrived(String topic, MqttMessage message) throws InterruptedException {
        System.out.println("topic: " + topic);
        System.out.println("Qos: " + message.getQos());
        messageContent = new String(message.getPayload());

        //获取本地时间
        LocalDateTime localTime = LocalDateTime.now();
        System.out.println("message content: " + messageContent);
        /*
         * 判断是上线还是掉线
         * */
        try {
            JSONObject jsonObject = JSON.parseObject(messageContent);
            String clientId = String.valueOf(jsonObject.get("clientid"));
            if (topic.endsWith("/disconnected")) {
                log.info("客户端已掉线：{}", clientId);
            } else {
                log.info("客户端已上线：{}", clientId);
            }
        } catch (JSONException e) {
            log.error("JSON Format Parsing Exception : {}", messageContent);
        }

        System.out.println("1: now: " + now + "  end: " + end);
        //断网处理,发布
        Duration duration;
        long l = 0;
        if (topic.equals("6666")) {
            end = LocalDateTime.now();
            end.plusSeconds(1);
            duration = Duration.between(now, end);
            l = duration.toMillis();
            System.out.println("2: now: " + now + "  end: " + end);
            System.out.println(l);
        }
        //发布
        if (l >= 15000) {
            Thread.sleep(1000);
            contentInter.testStatus(now, end);
        }
        end=LocalDateTime.now();
        now = end;
        System.out.println("3: now: " + now + "  end: " + end);
        int h, m, s;
        Long time;
        Date data = new Date();
        //断网处理，订阅
        //在这调用Service方法
        //判断Topic进行
        switch (topic) {
            case "mqtt/realLight":
                ReallDto rl = JSON.parseObject(messageContent, ReallDto.class);
                TbReall tbReall = new TbReall();
                tbReall.setLxin(rl.getLxin());
                if(null!=rl.getTime())
                {
                    time = rl.getTime();
                    h = (int) (time / 3600);
                    m = (int) (time / 60 % 60);
                    s = (int) (time % 60);
                    data.setHours(h);
                    data.setMinutes(m);
                    data.setSeconds(s);
                    Instant instant = data.toInstant();
                    LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
                    System.out.println(localDateTime);
                    tbReall.setTime(localDateTime);
                }else {
                    tbReall.setTime(LocalDateTime.now());
                }
                tbSave.saveLight(tbReall);
                break;
            case "mqtt/realTemp":
                RealtDto rt = JSON.parseObject(messageContent, RealtDto.class);
                TbRealt tbRealt = new TbRealt();
                tbRealt.setTout(rt.getTout());
                tbRealt.setTin(rt.getTin());

                if(null!=rt.getTime()) {
                    time = rt.getTime();
                    h = (int) (time / 3600 % 24);
                    m = (int) (time / 60 % 60);
                    s = (int) (time % 60);

                    data.setHours(h);
                    data.setMinutes(m);
                    data.setSeconds(s);
                    Instant instant2 = data.toInstant();
                    LocalDateTime localDateTime2 = LocalDateTime.ofInstant(instant2, ZoneId.systemDefault());
                    System.out.println(localDateTime2);
                    tbRealt.setTime(localDateTime2);
                }else {
                    tbRealt.setTime(LocalDateTime.now());
                }
                tbSave.saveTemp(tbRealt);
                break;
            case "mqtt/DeviceStatus":
                TbVstatus tbVstatus = JSON.parseObject(messageContent, TbVstatus.class);
                tbVstatus.setStatustime(LocalDateTime.now());
                tbSave.saveVStatus(tbVstatus);
                break;
            case "mqtt/TransportCabin":
                TbStorage tbStorage = JSON.parseObject(messageContent, TbStorage.class);
                tbSave.saveStorage(tbStorage);
                break;
            default:
                System.out.println("unknown topic");
                break;
        }

    }


    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("deliveryComplete---------" + token.isComplete());
    }
}
