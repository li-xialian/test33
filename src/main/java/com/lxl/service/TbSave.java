package com.lxl.service;


import com.lxl.model.po.*;

public interface TbSave {

    boolean saveLight(TbReall tbReall);
    boolean saveStorage(TbStorage tbStorage);
    boolean saveTemp(TbRealt tbRealt);
    boolean saveVStatus(TbVstatus tb);

    boolean saveControl(TbContorl tb);
}
