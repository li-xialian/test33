package com.lxl.service.impl;

import com.lxl.mapper.*;
import com.lxl.model.po.*;
import com.lxl.service.TbSave;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TbSaveImpl implements TbSave {
    @Autowired
    TbReallMapper tbReallMapper;
    @Autowired
    TbRealtMapper tbRealtMapper;
    @Autowired
    TbStorageMapper tbStorageMapper;
    @Autowired
    TbVstatusMapper tbVstatusMapper;
    @Autowired
    TbContorlMapper tbContorlMapper;



    @Override
    public boolean saveLight(TbReall tbLight) {
        int insert = tbReallMapper.insert(tbLight);
        if (insert>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean saveStorage(TbStorage t) {
        int f=0;
//        LambdaQueryWrapper<TbStorage> qu=new LambdaQueryWrapper<>();
//        qu.eq(TbStorage::getPid,t.getPid());
//        TbStorage exist = tbStorageMapper.selectOne(qu);
//        if(exist!=null){
//            return false;
//        }else {
//            f = tbStorageMapper.insert(t);
//        }
        f = tbStorageMapper.insert(t);
        if(f>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean saveTemp(TbRealt tb) {
        int insert = tbRealtMapper.insert(tb);
        if(insert>0){
            return true;
        }
        return false;
    }


    @Override
    public boolean saveVStatus(TbVstatus tb) {
        int f = tbVstatusMapper.insert(tb);

        if(f>0){
            return true;
        }
        return false;
    }

    @Override
    public boolean saveControl(TbContorl tb) {
        int f = tbContorlMapper.insert(tb);
        if(f>0){
            return true;
        }
        return false;
    }



}
