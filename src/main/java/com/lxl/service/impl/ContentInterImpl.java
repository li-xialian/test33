package com.lxl.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lxl.mapper.*;
import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.dto.PublishDto;
import com.lxl.model.dto.chat.CharDto;
import com.lxl.model.dto.chat.SeriesDto;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.po.*;
import com.lxl.mqtt.MqttMessageService;
import com.lxl.service.ContentInter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lxl
 */
@Slf4j
@Service
public class ContentInterImpl implements ContentInter {

    @Autowired
    TbReallMapper tbReallMapper;
    @Autowired
    TbRealtMapper tbRealtMapper;
    @Autowired
    TbStorageMapper tbStorageMapper;
    @Autowired
    TbVstatusMapper tbVstatusMapper;
    @Autowired
    TbContorlMapper tbContorlMapper;
    @Autowired
    MqttMessageService mqttMessageService;

    @Override
    public List<TbStorage> getTbStorage() {
        Wrapper<TbStorage> wrapper = new QueryWrapper<>();
        List<TbStorage> tbStorages = tbStorageMapper.selectList(wrapper);
        Collections.reverse(tbStorages);
        return tbStorages;

    }

    @Override
    public List<TbContorl> getContorl() {
        Wrapper<TbContorl> wrapper = new QueryWrapper<>();
        List<TbContorl> tbStorages = tbContorlMapper.selectList(wrapper);
        Collections.reverse(tbStorages);
        return tbStorages;

    }

    @Override
    public  List<TbContorl> getDurContorl(LocalDateTime now, LocalDateTime end) {
        LambdaQueryWrapper<TbContorl> wrapper = new LambdaQueryWrapper<>();
        wrapper.ge(TbContorl::getTime, now)
                .le(TbContorl::getTime, end)
                .orderByDesc(TbContorl::getTime);
        List<TbContorl> tb = tbContorlMapper.selectList(wrapper);
//        if (tb.size() != 0) {
//            TbContorl tbContorl = tb.get(0);
//            System.out.println(tbContorl);
//            return tbContorl;
//        }
        return tb;

    }

    @Override
    public Boolean testStatus(LocalDateTime now, LocalDateTime end) {
        List<TbContorl> durContorl = getDurContorl(now, end);
        System.out.println(durContorl);
        if (durContorl != null) {
            for(TbContorl tb:durContorl) {
                mqttMessageService.publish("mqtt/Control", tb);
            }
        }
        return true;
    }

    @Override
    public CharDto chat1(Integer size) {
        CharDto charDto = new CharDto();
        //1光照，2温度
        LambdaQueryWrapper<TbReall> wrapper1 = new LambdaQueryWrapper<>();
        wrapper1.orderByDesc(TbReall::getTime)
                .last("LIMIT " + size);
        List<TbReall> tb1 = tbReallMapper.selectList(wrapper1);

        //赋值1
        List<Float> c1 = tb1.stream().map(TbReall::getLxin).collect(Collectors.toList());
        List<LocalDateTime> time1 = tb1.stream().map(TbReall::getTime).collect(Collectors.toList());

        List<SeriesDto> series1 = new ArrayList<>();
        SeriesDto s1 = new SeriesDto();

        s1.setName("实时光照");
        s1.setData(c1);
        series1.add(s1);

        charDto.setSeries(series1);
        charDto.setCategories(time1);

        System.out.println("表1aaaaa:" + charDto);

        return charDto;
    }

    @Override
    public CharDto chat2(Integer size) {
        CharDto charDto = new CharDto();
        LambdaQueryWrapper<TbRealt> wrapper2 = new LambdaQueryWrapper<>();
        wrapper2.orderByDesc(TbRealt::getTime)
                .last("LIMIT " + size);
        List<TbRealt> tb2 = tbRealtMapper.selectList(wrapper2);

        //赋值2
        List<Float> c21 = tb2.stream().map(TbRealt::getTin).collect(Collectors.toList());
        List<Float> c22 = tb2.stream().map(TbRealt::getTout).collect(Collectors.toList());
        List<LocalDateTime> time2 = tb2.stream().map(TbRealt::getTime).collect(Collectors.toList());
        List<SeriesDto> series2 = new ArrayList<>();

        SeriesDto s21 = new SeriesDto();
        SeriesDto s22 = new SeriesDto();

        s21.setName("厢内温度");
        s21.setData(c21);
        s22.setName("厢外温度");
        s22.setData(c22);

        series2.add(s21);
        series2.add(s22);

        charDto.setSeries(series2);
        charDto.setCategories(time2);
        System.out.println("表2aaaaa:" + charDto);
        return charDto;
    }


    @Override
    public PageResult<TbReall> getRealLight(DateTimeDtos dateTimeDtos) {
        LambdaQueryWrapper<TbReall> wrapper = new LambdaQueryWrapper<>();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        if (start != null && end != null) {
            wrapper.ge(TbReall::getTime, start)
                    .le(TbReall::getTime, end)
                    .orderByDesc(TbReall::getTime);
        }
        Page<TbReall> page = new Page<>();
        if (null != dateTimeDtos.getPageCurrent()) {
            page = new Page<>(dateTimeDtos.getPageCurrent(), dateTimeDtos.getPageSize());
        }
        Page<TbReall> tbReallPage = tbReallMapper.selectPage(page, wrapper);
        List<TbReall> records = tbReallPage.getRecords();
        long total = tbReallPage.getTotal();
        //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
        PageResult<TbReall> tbReallPageResult;
        if (null != dateTimeDtos.getPageCurrent()) {
            tbReallPageResult = new PageResult<>(records, total, dateTimeDtos.getPageCurrent(), dateTimeDtos.getPageSize());
        } else {
            tbReallPageResult = new PageResult<>(records);
        }

        return tbReallPageResult;
    }

    @Override
    public PageResult<TbRealt> getRealTemperature(DateTimeDtos dateTimeDtos) {
        LambdaQueryWrapper<TbRealt> wrapper = new LambdaQueryWrapper<>();
        String start = dateTimeDtos.getStart();
        String end = dateTimeDtos.getEnd();
        if (start != null && end != null) {
            wrapper.ge(TbRealt::getTime, start)
                    .le(TbRealt::getTime, end)
                    .orderByDesc(TbRealt::getTime);
        }
        Page<TbRealt> page = new Page<>();
        if (null != dateTimeDtos.getPageCurrent()) {
            page = new Page<>(dateTimeDtos.getPageCurrent(), dateTimeDtos.getPageSize());
        }
        Page<TbRealt> tbReallPage = tbRealtMapper.selectPage(page, wrapper);
        List<TbRealt> records = tbReallPage.getRecords();
        long total = tbReallPage.getTotal();
        PageResult<TbRealt> tbReallPageResult;
        if (null != dateTimeDtos.getPageCurrent()) {
            //需要的参数List<T> items,  total,  getPageCurrent,  pageSize
            tbReallPageResult = new PageResult<>(records, total, dateTimeDtos.getPageCurrent(), dateTimeDtos.getPageSize());
        } else {
            tbReallPageResult = new PageResult<>(records);
        }
        return tbReallPageResult;
    }

    @Override
    public List<TbVstatus> getVStatus(String start, String end) {
        LambdaQueryWrapper<TbVstatus> wrapper = new LambdaQueryWrapper<>();
        if (start != null && end != null) {
            wrapper.ge(TbVstatus::getStatustime, start)
                    .le(TbVstatus::getStatustime, end)
                    .orderByDesc(TbVstatus::getStatustime);

        }
        List<TbVstatus> tbRealTemperatures = tbVstatusMapper.selectList(wrapper);
        return tbRealTemperatures;
    }
}
