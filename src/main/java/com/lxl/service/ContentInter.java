package com.lxl.service;

import com.lxl.model.dto.DateTimeDtos;
import com.lxl.model.dto.chat.CharDto;
import com.lxl.model.dto.pages.PageResult;
import com.lxl.model.po.*;


import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lxl
 * @since 2023-07-25
 */
public interface ContentInter {

    /**
     * 分页无条件查询
     * @return 查询的储运厢表结果
     */
    List<TbStorage> getTbStorage();


    List<TbContorl> getContorl();

    List<TbContorl> getDurContorl(LocalDateTime now, LocalDateTime start);


    Boolean testStatus(LocalDateTime now, LocalDateTime end);

    CharDto chat1(Integer size);

    CharDto chat2(Integer size);

    /**
     * 查询实时光照数据

     * @param start
     * @param end
     * @return
     */
//    CharDto getRealLight(String start, String end);
//    List<TbReall> getRealLight(String start, String end);

    /**
     * 查询实时温度数据
     * @return
     */
//    CharDto getRealTemperature(String start, String end);
//    List<TbRealt> getRealTemperature(String start, String end);

    PageResult<TbReall> getRealLight(DateTimeDtos dateTimeDtos);

    PageResult<TbRealt> getRealTemperature(DateTimeDtos dateTimeDtos);

    /**
     * 查询设备状态

     * @param start
     * @param end
     * @return
     */
    List<TbVstatus> getVStatus( String start, String end);

}
