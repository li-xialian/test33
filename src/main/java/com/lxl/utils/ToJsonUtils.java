package com.lxl.utils;

import org.springframework.stereotype.Component;

@Component
public class ToJsonUtils {

    public String getJson(String s){
//        String s = "222:555,555:hiuh,212:hjsh";
        String[] split = s.split(",");
        int length = split.length;
        int i=1;
        String json="";
        for(String spM:split){
            String[] split1 = spM.split(":");
            int j=1;
            for (String spF: split1){
                json=json+"\""+spF+"\"";
                if(j!=2){
                    json=json+":";
                }
                j++;
            }
            if(i!=length){
                json=json+",";
            }
            i++;
        }
        System.out.println(json);
        return json;
    }

}
